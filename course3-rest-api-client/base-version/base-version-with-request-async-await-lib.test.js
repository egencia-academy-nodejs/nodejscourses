const getBaseVersion = require('./base-version-with-request-async-await-lib')
const nock = require('nock')

nock.disableNetConnect()

function mockFlightServiceBaseVersion() {
  nock('https://www.egencia.com')
    .get('/flight-service/base/version')
    .reply(200, {
      active_profiles: 'prod,prodch,ancillaries,oms',
      application_data: { step_bom_version: '8.6.2' },
      application_name: 'flight-service',
      build_date: '2018-11-27T09:51:044Z',
      build_number: '80f69e87e1dfa87289d9cfd92a806b2730c71065',
      build_version: '2018-11-27-09-51-80f69e87e',
      machine_name: 'chexatsect007'
    })
}

describe('getBaseVersion', () => {
  it('should get base version of flight-service', async () => {
    mockFlightServiceBaseVersion()
    const result = await getBaseVersion(
      'https://www.egencia.com/flight-service/base/version'
    )
    expect(result).toEqual({
      active_profiles: 'prod,prodch,ancillaries,oms',
      application_data: { step_bom_version: '8.6.2' },
      application_name: 'flight-service',
      build_date: '2018-11-27T09:51:044Z',
      build_number: '80f69e87e1dfa87289d9cfd92a806b2730c71065',
      build_version: '2018-11-27-09-51-80f69e87e',
      machine_name: 'chexatsect007'
    })
  })
})
