const request = require('util').promisify(require('request'))

async function getBaseVersion(baseVersionUrl) {
  const response = await request(baseVersionUrl)
  return JSON.parse(response.body)
}

module.exports = getBaseVersion
