const request = require('util').promisify(require('request'))

function getBaseVersion() {
  request('https://www.egencia.com/flight-service/base/version')
    .then(response => {
      console.log(response.body)
    })
    .catch(err => console.error(err))
}

getBaseVersion()
