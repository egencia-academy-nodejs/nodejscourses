const request = require('util').promisify(require('request'))

async function getBaseVersion() {
  try {
    const response = await request(
      'https://www.egencia.com/flight-service/base/version'
    )
    console.log(response.body)
  } catch (err) {
    console.error(err)
  }
}

getBaseVersion()
