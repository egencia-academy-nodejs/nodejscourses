const request = require('request')

function getBaseVersion() {
  request(
    'https://www.egencia.com/flight-service/base/version',
    (err, response) => {
      if (err) {
        console.error(err)
      } else {
        console.log(response.body)
      }
    }
  )
}

getBaseVersion()
