const https = require('https')

function getBaseVersion() {
  https
    .request(
      'https://www.egencia.com/flight-service/base/version',
      {
        method: 'GET'
      },
      res => {
        let body = ''
        console.log(`STATUS: ${res.statusCode}`)
        console.log(`HEADERS: ${JSON.stringify(res.headers)}`)
        res.setEncoding('utf8')
        res.on('data', chunk => {
          console.log(`BODY: ${chunk}`)
          body += chunk
        })
        res.on('end', () => {
          console.log('No more data in response.')
          console.log(body)
        })
      }
    )
    .on('error', err => console.error(err))
    .end()
}

getBaseVersion()
