const nock = require('nock')
nock.disableNetConnect()
const {
  fetchUsers,
  fetchUser,
  createUser,
  updateUser,
  deleteUser,
} = require('./request-async-await-version')

test('it should list users', async () => {
  expect.assertions(1)
  const response = {
    data: [
      {
        id: 4,
        first_name: 'Eve',
        last_name: 'Holt',
        avatar:
          'https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg',
      },
      {
        id: 5,
        first_name: 'Charles',
        last_name: 'Morris',
        avatar:
          'https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg',
      },
    ],
  }
  nock('https://reqres.in')
    .get('/api/users')
    .reply(200, response)

  const users = await fetchUsers()
  expect(users).toEqual(response.data)
})

test('it should get a single user', async () => {
  expect.assertions(1)
  const response = {
    data: {
      id: 2,
      first_name: 'Janet',
      last_name: 'Weaver',
      avatar:
        'https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg',
    },
  }
  nock('https://reqres.in')
    .get('/api/users/2')
    .reply(200, response)

  const user = await fetchUser(2)
  expect(user).toEqual(response.data)
})

test('it should create a user', async () => {
  expect.assertions(1)
  const response = {
    name: 'egencia',
    job: 'dev',
    id: '666',
    createdAt: '2018-12-01T09:41:15.172Z',
  }
  nock('https://reqres.in')
    .post('/api/users', { name: 'egencia', job: 'dev' })
    .reply(201, response)

  const user = await createUser({ name: 'egencia', job: 'dev' })
  expect(user).toEqual(response)
})

test('it should update a user', async () => {
  expect.assertions(1)
  const response = {
    name: 'egencia',
    job: 'dev',
    updatedAt: '2018-12-01T09:49:11.836Z',
  }
  nock('https://reqres.in')
    .put('/api/users/1', { name: 'egencia', job: 'dev' })
    .reply(200, response)

  const user = await updateUser({ id: 1, name: 'egencia', job: 'dev' })
  expect(user).toEqual(response)
})

test('it should delete a user', async () => {
  expect.assertions(1)
  nock('https://reqres.in')
    .delete('/api/users/42')
    .reply(204)

  const deleteDone = await deleteUser(42)
  expect(deleteDone).toEqual(true)
})
