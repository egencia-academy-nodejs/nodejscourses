const request = require('request')

const reqresApiBasePath = 'https://reqres.in/api/users'

function fetchUsers(callback) {
  request(
    { uri: reqresApiBasePath, json: true, method: 'GET' },
    (err, { body: { data: users } }) => {
      callback(err, users)
    },
  )
}

function fetchUser(id, callback) {
  request(
    { uri: `${reqresApiBasePath}/${id}`, json: true, method: 'GET' },
    (err, { body: { data: user } }) => {
      callback(err, user)
    },
  )
}

function createUser({ name, job }, callback) {
  request(
    {
      uri: reqresApiBasePath,
      method: 'POST',
      json: {
        name,
        job,
      },
    },
    (err, { body }) => {
      callback(err, body)
    },
  )
}

function updateUser({ id, name, job }, callback) {
  request(
    {
      uri: `${reqresApiBasePath}/${id}`,
      method: 'PUT',
      json: { name, job },
    },
    (err, { body }) => {
      callback(err, body)
    },
  )
}

function deleteUser(id, callback) {
  request(
    {
      uri: `${reqresApiBasePath}/${id}`,
      method: 'DELETE',
    },
    (err, { statusCode }) => {
      callback(err, statusCode === 204)
    },
  )
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
  updateUser,
  deleteUser,
}
