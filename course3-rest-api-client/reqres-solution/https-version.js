const https = require('https')

const reqresApi = {
  hostname: 'reqres.in',
  path: '/api/users',
}

function fetchUsers(callback) {
  let result = ''
  https
    .get(reqresApi, res => {
      res
        .on('data', chunk => {
          result += chunk
        })
        .on('end', () => {
          const { data: users } = JSON.parse(result)
          callback(null, users)
        })
    })
    .on('error', e => {
      callback(new Error('Error while fetching users'))
    })
}

function fetchUser(id, callback) {
  const options = { ...reqresApi, path: '/api/users/' + id }
  let result = ''
  https
    .get(options, res => {
      res
        .on('data', chunk => {
          result += chunk
        })
        .on('end', () => {
          const { data: user } = JSON.parse(result)
          callback(null, user)
        })
    })
    .on('error', e => {
      callback(new Error('Error while fetching user ' + id))
    })
}

function createUser({ name, job }, callback) {
  const options = {
    ...reqresApi,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  }

  let result = ''
  const request = https.request(options, res => {
    res
      .on('data', data => {
        result += data
      })
      .on('end', () => {
        const created = JSON.parse(result)
        callback(null, created)
      })
  })

  request.write(JSON.stringify({ name, job }))
  request.end()
}

function updateUser({ id, name, job }, callback) {
  const options = {
    ...reqresApi,
    path: '/api/users/' + id,
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
  }

  let result = ''
  const request = https.request(options, res => {
    res
      .on('data', data => {
        result += data
      })
      .on('end', () => {
        const created = JSON.parse(result)
        callback(null, created)
      })
  })

  request.write(JSON.stringify({ name, job }))
  request.end()
}

function deleteUser(id, callback) {
  const options = { ...reqresApi, path: '/api/users/' + id, method: 'DELETE' }

  https
    .get(options, res => {
      callback(null, res.statusCode === 204)
    })
    .on('error', e => {
      callback(new Error('Error while delete user ' + id))
    })
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
  updateUser,
  deleteUser,
}
