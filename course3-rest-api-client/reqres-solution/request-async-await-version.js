const request = require('util').promisify(require('request'))

const reqresApiBasePath = 'https://reqres.in/api/users'

async function fetchUsers() {
  const {
    body: { data: users },
  } = await request({ uri: reqresApiBasePath, json: true, method: 'GET' })
  return users
}

async function fetchUser(id) {
  const {
    body: { data: user },
  } = await request({
    uri: `${reqresApiBasePath}/${id}`,
    json: true,
    method: 'GET',
  })
  return user
}

async function createUser({ name, job }) {
  const { body } = await request({
    uri: reqresApiBasePath,
    method: 'POST',
    json: {
      name,
      job,
    },
  })
  return body
}

async function updateUser({ id, name, job }) {
  const { body } = await request({
    uri: `${reqresApiBasePath}/${id}`,
    method: 'PUT',
    json: { name, job },
  })
  return body
}

async function deleteUser(id) {
  const { statusCode } = await request({
    uri: `${reqresApiBasePath}/${id}`,
    method: 'DELETE',
  })
  return statusCode === 204
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
  updateUser,
  deleteUser,
}
