const wretch = require('wretch')

wretch().polyfills({
  fetch: require('node-fetch'),
  FormData: require('form-data'),
  URLSearchParams: require('url').URLSearchParams,
})

const logMiddleware = () => next => (url, opts) => {
  console.log(opts.method + '@' + url)
  return next(url, opts)
}

const reqresApi = wretch('https://reqres.in/api/users').middlewares([
  logMiddleware(),
])

async function fetchUsers() {
  const { data: users } = await reqresApi.get().json()
  return users
}

async function fetchUser(userId) {
  const { data: user } = await reqresApi
    .url(`/${userId}`)
    .get()
    .json()
  return user
}

async function createUser({ name, job }) {
  return reqresApi.post({ name, job }).json()
}

async function updateUser({ id, name, job }) {
  return reqresApi
    .url(`/${id}`)
    .put({ name, job })
    .json()
}

async function deleteUser(userId) {
  const { status } = await reqresApi
    .url(`/${userId}`)
    .delete()
    .res()
  return status === 204
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
  updateUser,
  deleteUser,
}
