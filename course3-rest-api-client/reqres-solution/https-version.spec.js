const nock = require('nock')
nock.disableNetConnect()
const {
  fetchUsers,
  fetchUser,
  createUser,
  updateUser,
  deleteUser,
} = require('./https-version')

test('it should list users', done => {
  expect.assertions(1)
  const response = {
    data: [
      {
        id: 4,
        first_name: 'Eve',
        last_name: 'Holt',
        avatar:
          'https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg',
      },
      {
        id: 5,
        first_name: 'Charles',
        last_name: 'Morris',
        avatar:
          'https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg',
      },
    ],
  }
  nock('https://reqres.in')
    .get('/api/users')
    .reply(200, response)

  function callback(err, result) {
    if (err) throw new Error()
    expect(result).toEqual(response.data)
    done()
  }

  fetchUsers(callback)
})

test('it should get a single user', done => {
  expect.assertions(1)
  const response = {
    data: {
      id: 2,
      first_name: 'Janet',
      last_name: 'Weaver',
      avatar:
        'https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg',
    },
  }
  nock('https://reqres.in')
    .get('/api/users/2')
    .reply(200, response)

  function callback(err, result) {
    if (err) throw new Error()
    expect(result).toEqual(response.data)
    done()
  }

  fetchUser(2, callback)
})

test('it should create a user', done => {
  expect.assertions(1)
  const response = {
    name: 'egencia',
    job: 'dev',
    id: '666',
    createdAt: '2018-12-01T09:41:15.172Z',
  }
  nock('https://reqres.in')
    .post('/api/users', { name: 'egencia', job: 'dev' })
    .reply(201, response)

  function callback(err, result) {
    if (err) throw new Error()
    expect(result).toEqual(response)
    done()
  }

  createUser({ name: 'egencia', job: 'dev' }, callback)
})

test('it should update a user', done => {
  expect.assertions(1)
  const response = {
    name: 'egencia',
    job: 'dev',
    updatedAt: '2018-12-01T09:49:11.836Z',
  }
  nock('https://reqres.in')
    .put('/api/users/2', { name: 'egencia', job: 'dev' })
    .reply(200, response)

  function callback(err, result) {
    if (err) throw new Error()
    expect(result).toEqual(response)
    done()
  }

  updateUser({ id: 2, name: 'egencia', job: 'dev' }, callback)
})

test('it should delete a user', done => {
  expect.assertions(1)
  nock('https://reqres.in')
    .delete('/api/users/42')
    .reply(204)

  function callback(err, result) {
    if (err) throw new Error()
    expect(result).toEqual(true)
    done()
  }

  deleteUser(42, callback)
})
