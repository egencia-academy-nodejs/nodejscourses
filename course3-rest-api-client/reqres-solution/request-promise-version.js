const request = require('util').promisify(require('request'))

const reqresApiBasePath = 'https://reqres.in/api/users'

function fetchUsers() {
  return request({ uri: reqresApiBasePath, json: true, method: 'GET' }).then(
    ({ body: { data: users } }) => users,
  )
}

function fetchUser(id) {
  return request({
    uri: `${reqresApiBasePath}/${id}`,
    json: true,
    method: 'GET',
  }).then(({ body: { data: user } }) => user)
}

function createUser({ name, job }) {
  return request({
    uri: reqresApiBasePath,
    method: 'POST',
    json: {
      name,
      job,
    },
  }).then(({ body }) => body)
}

function updateUser({ id, name, job }) {
  return request({
    uri: `${reqresApiBasePath}/${id}`,
    method: 'PUT',
    json: { name, job },
  }).then(({ body }) => body)
}

function deleteUser(id) {
  return request({
    uri: `${reqresApiBasePath}/${id}`,
    method: 'DELETE',
  }).then(({ statusCode }) => statusCode === 204)
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
  updateUser,
  deleteUser,
}
