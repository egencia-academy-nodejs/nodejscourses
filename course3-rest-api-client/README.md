# Fetching api

Slides: https://slides.com/rodolphebung/nodejs-academy-fetching-apis

## Before exercise

Run `npm install` on the root project before starting the exercise. This command install several dependencies used by this exercise:

- ESLint
- Jest
- Prettier
- Nock
- Request
- Wretch (form-data, node-fetch)

Nice to have: install VSCode plugin `ESLint`, `Prettier - Code formatter` and `Jest` and `Jest Snippets` to help you work better under VSCode.

## Using Callback with http/https

Code example: [https-version.js](./reqres/https-version.js).

Exercise: Implement `createUser` with HTTP POST. HTTP Example:

```text
API Endpoint: https://reqres.in/api/users
Method HTTP: POST
POST Data example:
{ name: 'egencia', job: 'dev' }
```

Test is present: [https-version.spec.js](./reqres/https-version.spec.js).

## Using Callback with request

Code example: [request-callback-version.js](./reqres/request-callback-version.js).

Exercise: Implement `createUser` with HTTP POST. HTTP Example:

```text
API Endpoint: https://reqres.in/api/users
Method HTTP: POST
POST Data example:
{ name: 'egencia', job: 'dev' }
```

Test is present: [request-callback-version.spec.js](./reqres/request-callback-version.spec.js).

## Using Promise with request

Code example: [request-promise-version.js](./reqres/request-promise-version.js).

Exercise: Implement `createUser` with HTTP POST. HTTP Example:

```text
API Endpoint: https://reqres.in/api/users
Method HTTP: POST
POST Data example:
{ name: 'egencia', job: 'dev' }
```

Test is present: [request-promise-version.spec.js](./reqres/request-promise-version.spec.js).

## Using async/await with request

Code example: [request-async-await-version.js](./reqres/request-async-await-version.js).

Exercise: Implement `createUser` with HTTP POST. HTTP Example:

```text
API Endpoint: https://reqres.in/api/users
Method HTTP: POST
POST Data example:
{ name: 'egencia', job: 'dev' }
```

Test is present: [request-async-await-version.spec.js](./reqres/request-async-await-version.spec.js).

## Using async/await with wretch

Code example: [wretch-version.js](./reqres/wretch-version.js).

Exercise: Implement `createUser` with HTTP POST. HTTP Example:

```text
API Endpoint: https://reqres.in/api/users
Method HTTP: POST
POST Data example:
{ name: 'egencia', job: 'dev' }
```

Test is present: [wretch-version.spec.js](./reqres/wretch-version.spec.js).

## Homework

Implement `updateUser`, `deleteUser` and tests.
