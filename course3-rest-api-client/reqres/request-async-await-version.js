const request = require('util').promisify(require('request'))

const reqresApiBasePath = 'https://reqres.in/api/users'

async function fetchUsers() {
  const {
    body: { data: users },
  } = await request({ uri: reqresApiBasePath, json: true, method: 'GET' })
  return users
}

async function fetchUser(id) {
  const {
    body: { data: user },
  } = await request({
    uri: `${reqresApiBasePath}/${id}`,
    json: true,
    method: 'GET',
  })
  return user
}

async function createUser({ name, job }) {
  // TODO Implement user creation
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
}
