const https = require('https')

const reqresApi = {
  hostname: 'reqres.in',
  path: '/api/users',
}

function fetchUsers(callback) {
  let result = ''
  https
    .get(reqresApi, res => {
      res
        .on('data', chunk => {
          result += chunk
        })
        .on('end', () => {
          const { data: users } = JSON.parse(result)
          callback(null, users)
        })
    })
    .on('error', e => {
      callback(new Error('Error while fetching users'))
    })
}

function fetchUser(id, callback) {
  const options = { ...reqresApi, path: '/api/users/' + id }
  let result = ''
  https
    .get(options, res => {
      res
        .on('data', chunk => {
          result += chunk
        })
        .on('end', () => {
          const { data: user } = JSON.parse(result)
          callback(null, user)
        })
    })
    .on('error', e => {
      callback(new Error('Error while fetching user ' + id))
    })
}

function createUser({ name, job }, callback) {
  // TODO Implement user creation
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
}
