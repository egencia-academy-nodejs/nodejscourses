const nock = require('nock')
nock.disableNetConnect()
const {
  fetchUsers,
  fetchUser,
  createUser,
} = require('./request-async-await-version')

test('it should list users', async () => {
  expect.assertions(1)
  const response = {
    data: [
      {
        id: 4,
        first_name: 'Eve',
        last_name: 'Holt',
        avatar:
          'https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg',
      },
      {
        id: 5,
        first_name: 'Charles',
        last_name: 'Morris',
        avatar:
          'https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg',
      },
    ],
  }
  nock('https://reqres.in')
    .get('/api/users')
    .reply(200, response)

  const users = await fetchUsers()
  expect(users).toEqual(response.data)
})

test('it should get a single user', async () => {
  expect.assertions(1)
  const response = {
    data: {
      id: 2,
      first_name: 'Janet',
      last_name: 'Weaver',
      avatar:
        'https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg',
    },
  }
  nock('https://reqres.in')
    .get('/api/users/2')
    .reply(200, response)

  const user = await fetchUser(2)
  expect(user).toEqual(response.data)
})

test('it should create a user', async () => {
  expect.assertions(1)
  const response = {
    name: 'egencia',
    job: 'dev',
    id: '666',
    createdAt: '2018-12-01T09:41:15.172Z',
  }
  nock('https://reqres.in')
    .post('/api/users', { name: 'egencia', job: 'dev' })
    .reply(201, response)

  const user = await createUser({ name: 'egencia', job: 'dev' })
  expect(user).toEqual(response)
})
