const request = require('util').promisify(require('request'))

const reqresApiBasePath = 'https://reqres.in/api/users'

function fetchUsers() {
  return request({ uri: reqresApiBasePath, json: true, method: 'GET' }).then(
    ({ body: { data: users } }) => users,
  )
}

function fetchUser(id) {
  return request({
    uri: `${reqresApiBasePath}/${id}`,
    json: true,
    method: 'GET',
  }).then(({ body: { data: user } }) => user)
}

function createUser({ name, job }) {
  // TODO Implement user creation
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
}
