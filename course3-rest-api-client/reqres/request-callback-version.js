const request = require('request')

const reqresApiBasePath = 'https://reqres.in/api/users'

function fetchUsers(callback) {
  request(
    { uri: reqresApiBasePath, json: true, method: 'GET' },
    (err, { body: { data: users } }) => {
      callback(err, users)
    },
  )
}

function fetchUser(id, callback) {
  request(
    { uri: `${reqresApiBasePath}/${id}`, json: true, method: 'GET' },
    (err, { body: { data: user } }) => {
      callback(err, user)
    },
  )
}

function createUser({ name, job }, callback) {
  // TODO Implement user creation
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
}
