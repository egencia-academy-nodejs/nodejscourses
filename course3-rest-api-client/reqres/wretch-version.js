const wretch = require('wretch')

wretch().polyfills({
  fetch: require('node-fetch'),
  FormData: require('form-data'),
  URLSearchParams: require('url').URLSearchParams,
})

const reqresApi = wretch('https://reqres.in/api/users')

async function fetchUsers() {
  const { data: users } = await reqresApi.get().json()
  return users
}

async function fetchUser(userId) {
  const { data: user } = await reqresApi
    .url(`/${userId}`)
    .get()
    .json()
  return user
}

async function createUser({ name, job }) {
  // TODO Implement user creation
}

module.exports = {
  fetchUsers,
  fetchUser,
  createUser,
}
