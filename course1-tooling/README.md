# Tooling

## General

https://slides.com/rodolphebung/nodejs-academy-tooling

## Workshop

### Install Node/Npm

- windows:
  - with nvm
    1. install nvm [link](https://github.com/coreybutler/nvm-windows#node-version-manager-nvm-for-windows)
    2. run `nvm install --lts` or `nvm install 10` for windows
  - With installer: [install link](https://nodejs.org/en/download/)
- mac:
  1. Install nvm: [link](https://github.com/creationix/nvm#install-script)
  2. run `nvm install --lts` or `nvm install 10` for windows

Then check node and npm version:

    node -v
    npm -v

<!--
? what's node
node is a binary executable that can execute a NodeJS program
? what's npm
npm is a package management tool for NodeJS
npmjs.com
-->

### Install VSCode

Install VSCode: [link](https://code.visualstudio.com/docs/setup/setup-overview)

<!--
? why VSCode
fast and easy to use
? why preconfigure environment
VSCode is powerful, with plugins
plugins are difficult to setup
-->

### Initialise your project

`npm init` and you will have to answer some questions about your project

or

`npm init -y` if you don't care 😏

### Install ESLint

1. Checkout branch `exercice1-eslint`: `git checkout exercice1-eslint`
2. Install ESLint: run `npm install eslint --save-dev`
3. Configure ESLint:
   - create a eslint configuration file `.eslintrc.js` in the root
   - edit `.eslintrc.js`: this add a rule for validating `typeof` function
     ```js
     module.exports = {
       parserOptions: {
         ecmaVersion: 2018,
       },
       rules: {
         'valid-typeof': 'error',
       },
     }
     ```
   - run `npx eslint ./src` and see what it says
   - change eslint configuration to standard style:
     1. run `npx eslint --init`
     2. for question `How would you like to configure ESLint?`, pick `Use a popular style guide`
     3. for question `Which style guide do you want to follow?`, pick `Standard`
     4. for question `What format do you want your config file to be in?`, pick `JavaScript`
     5. for question `Would you like to install them now with npm?`, pick `Yes`
     6. add rule : `"no-console": "off"`
   - run `npx eslint ./src --fix` and see what are changed in `./src/example.js`

Final configuration can be found in the branch: `exercice1-eslint-correction`

```bash
git checkout exercice1-eslint-correction
```

### Install Prettier

1. Checkout branch: `exercice2-prettier`: `git checkout exercice2-prettier`
2. Install Prettier: `npm install --save-dev prettier`
3. Add a prettier configuration file `.prettierrc`:

   ```json
   {
     "semi": false,
     "singleQuote": true
   }
   ```

4. Run `npx prettier ./src/example.js` and see what's the output
5. Run `npx prettier --write ./src/example.js` to make the formatted written in `example.js`

Final configuration can be found in the branch: `exercice2-prettier-correction`

```bash
git checkout exercice2-prettier-correction
```

### Make ESLint/Prettier/VSCode work together

Since Prettier and ESLint have some conflit on the formatting the code, we'll try to make them work together:

Before starting, checkout the branch: `exercice3-make-eslint-prettier-work-together`

#### ESLint + Standard + Prettier

1. Run: `npm install eslint prettier --save-dev`
2. add or edit the `.eslintrc.js` file:

   ```js
   module.exports = {
     parserOptions: {
       ecmaVersion: 2018,
     },
     extends: ['standard'],
     rules: {
       'no-console': 'off',
     },
     env: {
       es6: true,
     },
   }
   ```

3. Run `npx prettier --write ./src/example.js` and `npx eslint ./src` to see what eslint says
4. Resolve the conflit: use ESLint as a linter and Prettier as the formatter

   1. Run `npm install --save-dev eslint-config-prettier eslint-config-standard eslint-plugin-import eslint-plugin-node eslint-plugin-prettier eslint-plugin-promise eslint-plugin-standard` to install necessary dependencies
   2. Add at the end of `extends` array in `.eslintrc.js`:

      ```text
      "prettier",
      "prettier/standard",
      ```

   3. add or edit the `.prettierrc` file.

      ```json
      {
        "semi": false,
        "singleQuote": true
      }
      ```

   4. Run `npx eslint ./src` again to see if conflit solved or not

#### Integrate the VSCode

1. Install different plugin: `ESLint` and `Prettier - Code formatter` in the extensions marketplace.
2. Restart VSCode
3. Do some changes in `./src/example.js`: add extract space and change

Final configuration can be found in the branch: `exercice3-make-eslint-prettier-work-together`

```bash
git checkout exercice3-make-eslint-prettier-work-together-correction
```

### Jest

1. Checkout branch `exercice4-jest`: `git checkout exercice4-jest`
2. The branch is pre-setup with all previous configuration: ESLint + Standard + Prettier
3. Check the project: `./src/example.js` and `./__tests__/example-test.js`
4. Install `jest` and eslint jest plugin `eslint-plugin-jest`: `npm install jest eslint-plugin-jest --save-dev`
5. Install VSCode Jest plugin: `Jest` and restart VSCode
6. Run Jest:
   1. with terminal: `npx jest`
   2. with VSCode:
      - add a vscode configuration: checkout the [launch.json](./.vscode/launch.json) in the correction branch `exercice4-jest-correction`

Final configuration can be found in the branch: `exercice4-jest-correction`

```bash
git checkout exercice4-jest-correction
```

### Implements Fizzbuzz (with tests !)

This program plays the game "Fizzbuzz".

It counts to 20, replacing each multiple of 3 with the word fizz, each multiple of 5 with the word buzz, and each multiple of both with the word fizzbuzz. otherwise it prints the number.

You can either expose a function and write a test, or print the number with console.log. Use your new-learned JS skills!

Tips:

1. Run NodeJS program: `node ./src/fizzbuzz.js`
2. You can debug the program with VSCode: [VSCode doc](https://code.visualstudio.com/docs/nodejs/nodejs-debugging)

Correction can be find in the branch `exercice5-fizzbuzz-correction`

```bash
git checkout exercice5-fizzbuzz-correction
```
