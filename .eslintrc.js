module.exports = {
  parserOptions: {
    ecmaVersion: 2018
  },
  extends: [
    'standard',
    'prettier',
    'prettier/standard',
    'plugin:jest/recommended'
  ],
  rules: {
    'no-console': 'off'
  },
  env: {
    es6: true,
    node: true
  }
}
