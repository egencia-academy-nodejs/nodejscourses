const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const helloApi = require('./hello')

app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.status(200).send('Express Hello World 🌎 example for Node academy')
})

app.use('/api/hello', helloApi)

module.exports = app
