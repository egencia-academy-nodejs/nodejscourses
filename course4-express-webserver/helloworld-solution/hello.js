const { Router } = require('express')
const router = new Router()

router.get('/', (req, res) => {
  res.status(200).json({ hello: 'world' })
})

router.post('/', (req, res) => {
  res.status(201).json({ hello: `${req.body.name}` })
})

module.exports = router
