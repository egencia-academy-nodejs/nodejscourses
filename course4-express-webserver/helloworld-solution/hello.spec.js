const request = require('supertest')
const app = require('./app')

it('it should "get" from hello API', () => {
  return request(app)
    .get('/api/hello')
    .expect(200)
    .then(res => {
      expect(res.body).toEqual({ hello: 'world' })
    })
})

it('it should "post" from hello API', () => {
  return request(app)
    .post('/api/hello')
    .send({ name: 'john' })
    .set('Accept', 'application/json')
    .expect(201)
    .then(res => {
      expect(res.body).toEqual({ hello: 'john' })
    })
})
