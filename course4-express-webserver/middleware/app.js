const express = require('express')
const app = express()

app.use('*/*', (req, res, next) => {
  console.log(`Time: ${Date.now()}`)
  next()
})

app.get('/', (req, res) => {
  res.send({ hello: 'world' })
})

module.exports = app
