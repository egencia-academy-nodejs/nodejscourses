const request = require('supertest')
const app = require('./app')

it('it should respond correctly on root', () => {
  return request(app)
    .get('/')
    .expect(200)
    .then(res => {
      expect(res.text).toEqual(
        'Express Hello World 🌎 example for Node academy',
      )
    })
})
