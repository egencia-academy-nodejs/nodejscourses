const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.status(200).send('Express Hello World 🌎 example for Node academy')
})

module.exports = app
