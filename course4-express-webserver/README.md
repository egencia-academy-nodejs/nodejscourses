# Create web service with express

Slides: <https://slides.com/rodolphebung/nodejs-academy-make-a-web-service>

## Before exercise

Install `nodemon` globally:

```bash
npm install -g nodemon
```

Then install project dependencies (on the root directory):

```bash
npm install
```

`nodemon` will help us reload the application and debug in VSCode.

There are 3 ways that you can launch the application in this course:

1. with `node`

   - example: `node helloworld/server.js`
   - pros: easy to launch without extract package installed
   - cons: cannot automatically reload the app, not easy to debug witout IDE

2. with `nodemon`

   - example: `nodemon helloworld/server.js`
   - pros: it reloads automatically the application when file changes
   - cons: need to be installed globally, cannot debug the app

3. with `VSCode + nodemon`

   - example: select the file to launch, go to Debug view and run the `nodemon` configuration
   - pros: it reloads app and we can debug the code
   - cons: need to install `nodemon` globally and configure VSCode

## Hello world

### Overview

1. Enter the directory `course4-express-webserver`.
2. Check code in [helloworld/server.js](./helloworld/server.js).
3. Launch application the way you want.
4. Ensure your application is running, you can :
   - use a `curl` command : `curl -v http://localhost:3000`
   - open your browser and go to `http://localhost:3000`

### Testing

To run the tests, simply run `npx jest`. If you want `watch` mode, you can type `npx jest --watch`

Take a look at [helloworld/app.spec.js](./helloworld/app.spec.js) and look how you can test your app with [supertest](https://github.com/visionmedia/supertest).

### Hello API

We will enrich our app with a very exciting feature : hello. 😏🤓

#### A `GET` endpoint

First, your app will need to be able to respond on a `GET` request on `/api/hello`. The response should be an hard coded JSON : `{ hello: 'world' }`

1. In `hello.spec.js` file, see the first test and unskip it to see it failed.
2. In `app.js`, add a route to respond on a `GET` request with the specified hard coded response `{ hello: 'world' }` and a `200` http status.
3. Ensure your code is OK by running the unit test ! 😊 Or you can check with a `curl` (your app must be running) : `curl -v http://localhost:3000/api/hello`

#### A `POST` endpoint

Let's go further : your app will need to respond on a `POST` requiest on the same API `/api/hello`. The payload should provide a JSON with a `name` property (e.g. : `{"name":"toto"}`). Then the server should respond in a more "dynamic" way 😉 : the response status should be `201` and the body should be like `{ hello: 'name' }` and the `name` should match the `name` provided in the request.

1. In `hello.spec.js` file, see the second test and unskip it to see it failed.
2. In `app.js`, add a route to respond on a `POST` request. Express is very minimalist and it is not easy to read the request payload. To do it in a more convenient way, you will have to install a package called [body-parser](https://github.com/expressjs/body-parser) and use [json method](https://github.com/expressjs/body-parser#bodyparserjsonoptions). Then you will be able to read the request `body` property. Ensure to respond with the good http status (`201`).
3. Run your test to ensure your code is OK. You can run this `curl` command too : `curl -v -d '{"name":"toto"}' -H "Content-Type: application/json" -X POST http://localhost:3000/api/hello`

#### Refactoring

Your API looks good but is located in the generic `app.js` file. If you plan to have more endpoints, you will want to split your routes in dedicated files. Let's write an `hello.js` file to put all our `hello` API!

1. Create an `hello.js` file
2. Copy the 2 routes relative to `hello` API from `app.js` into `hello.js`
3. You will have to use the `Router` constructor from `express` to have an instance (let's call it `router`)

   ```js
   const { Router } = require('express')
   const router = new Router()
   ```

4. Use the `router` instead of the `app` for your routes
5. Instead of using `api/hello` for your routes, use `/` (you will see why later 🤓)
6. Finally export your router : `module.exports = router`
7. In `app.js` import the previous `hello` router : `const helloApi = require('./hello')`
8. Remove the routes relative to the `hello` API
9. Just after the `/` route, tell your app to use your `hello` API routes : `app.use('/api/hello', helloApi)`
10. Ensure your app is still working as expected with your tests !

## Middleware: response time and logger

1. Enter the directory `course4-express-webserver`.
2. Check code in [middleware/server.js](./middleware/server.js).
3. Launch application with the way you want.

Exercise: try to log the requested URL and response time of the request.

### Third party middleware

cookie-parser: link [cookie-parser](https://expressjs.com/en/resources/middleware/cookie-parser.html).

Exercise: explore [cookie-parser](https://expressjs.com/en/resources/middleware/cookie-parser.html) to create an endpoint `/read-cookie` which displays the content of cookies.
