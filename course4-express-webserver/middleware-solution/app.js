const express = require('express')
const cookieParser = require('cookie-parser')
const app = express()

app.use(cookieParser())

app.use('*/*', (req, res, next) => {
  req.beginTime = Date.now()
  console.log(`Time: ${req.beginTime}`)
  console.log(`request url: ${req.baseUrl} and request method: ${req.method}`)
  next()
})

app.get('/', (req, res) => {
  console.log(`Total escape time: ${Date.now() - req.beginTime}`)
  res.send({ hello: 'world' })
})

app.get('/read-cookie', (req, res) => {
  console.log(req.cookies)
  res.send(req.cookies)
})

module.exports = app
