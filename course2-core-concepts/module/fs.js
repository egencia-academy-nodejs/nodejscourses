const fs = require('fs')
const readFile = require('util').promisify(fs.readFile)

readFile(`${__dirname}/module-usage/toto.json`, 'utf-8')
  .then(data => console.log(data))
  .catch(err => console.error(err))
