const libDefault = require('./lib-default-exports')
const { foo, bar, toto } = require('./lib-named-exports')

libDefault()
foo()
bar()
toto()
