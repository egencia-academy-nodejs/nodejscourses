function waitFor1SecSynchronously(params) {
  const startCallback = Date.now()
  while (Date.now() - startCallback < 1000) {
    // do nothing
  }
}

function resolveRightNowAsync(callback) {
  setTimeout(() => {
    console.log(2)
    waitFor1SecSynchronously()
    Math.random() <= 0.5
      ? callback(null, 'success')
      : callback(new Error('failure'))
  }, 0)
}

function asyncCall() {
  console.log(1)
  resolveRightNowAsync((err, result) => {
    if (err) {
      console.log(`3 ${err}`)
    } else {
      console.log(`3 ${result}`)
    }
  })
  console.log(4)
}

asyncCall()
