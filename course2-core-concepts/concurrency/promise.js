function waitFor1SecSynchronously(params) {
  const startCallback = Date.now()
  while (Date.now() - startCallback < 1000) {
    // do nothing
  }
}

function resolveRightNowAsync() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(2)
      waitFor1SecSynchronously()
      Math.random() <= 0.5 ? resolve('success') : reject(new Error('failure'))
    }, 0)
  })
}

function asyncCall() {
  console.log(1)
  resolveRightNowAsync()
    .then(result => console.log(`3 ${result}`))
    .catch(err => console.log(`3 ${err}`))
  console.log(4)
}

asyncCall()
