# Interact with database

Slides: https://slides.com/rodolphebung/interact-with-a-database

## Before exercise

1. Install a mongo client.
2. Go to project directory and run:

   ```bash
   npm install
   ```

3. Go to the directory `course5-interact-with-database`

## Explore CRUD with mongoose

Example of mongoose can be found in [connection-schema/example.js](./connection-schema/example.js)

The idea is to learn how to do CRUD with mongoose with a pokeball repository app. The app can:

1. Create a type of pokeball with a default stock `0`.
2. It can list all pokeball that the repository have.
3. It can increase and decrease the stock of a pokeball by `1` each time.
4. It can reset the stock of a pokeball to `0`.
5. It can delete a pokeball from the repository.

Data structure of our pokeball:

```js
{
  id: ObjectId,// handled already by mongoose
  name: 'MasterBall',// the name of the pokeball
  stock: 1,// the stock of the pokeball, default 0
}
```

> mongoose find return a mongoose model, not a json. You need to use `.lean()` to tell mongoose return a json instead of mongoose model!

### Connect to MongoDB and create schema with mongoose

Check the code in [connection-schema/example.js](./connection-schema/example.js).

### Note

At this point, you can run tests relative to this course with this command

```bash
npx jest course5-interact-with-database/pokeball --watch
```

Make them all pass with the exercices below ! 💪

### Insert

Implement `createPokeball` in [pokeball/app.js](./pokeball/app.js): Create a type of pokeball.

### Read

Implement `listAllPokeballs` in [pokeball/app.js](./pokeball/app.js): List all pokeballs that the repo have.

### Update

Implement `increasePokeball` in [pokeball/app.js](./pokeball/app.js): Increase the stock of a given pokeball.

Implement `decreasePokeball` in [pokeball/app.js](./pokeball/app.js): Decrease the stock of a given pokeball.

### Delete

Implement `deletePokeball` in [pokeball/app.js](./pokeball/app.js): Delete a pokeball.
