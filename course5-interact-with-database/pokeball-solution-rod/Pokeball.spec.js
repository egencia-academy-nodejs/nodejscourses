const { MongoMemoryServer } = require('mongodb-memory-server')
const Pokeball = require('./Pokeball')
const mongoose = require('mongoose')

let mongod

beforeEach(async () => {
  mongod = new MongoMemoryServer()
})

afterEach(async () => {
  mongod.stop()
})

it('should create a single pokeball', async () => {
  // Given
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)

  // When
  const name = 'Poke Ball'
  const pokeball = new Pokeball({ name })
  await pokeball.save()

  // Then
  const pokeballs = await Pokeball.find(
    {},
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([{ name: 'Poke Ball', stock: 0 }])
})

it('should list all pokeballs', async () => {
  // Given
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  await Pokeball.insertMany([
    { name: 'Poke Ball' },
    { name: 'Master Ball' },
    { name: 'Ultra Ball' },
  ])

  // When
  const pokeballs = await Pokeball.find(
    {},
    { _id: 0, name: 1, stock: 1 },
  ).lean()

  // Then
  expect(pokeballs).toEqual([
    { name: 'Poke Ball', stock: 0 },
    { name: 'Master Ball', stock: 0 },
    { name: 'Ultra Ball', stock: 0 },
  ])
})

it('should increase the stock of pokeball', async () => {
  // Given
  const pokeballName = 'Poke Ball'
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  const pokeball = new Pokeball({ name: pokeballName })
  await pokeball.save()

  // When
  await pokeball.increase()

  // Then
  const pokeballs = await Pokeball.find(
    { name: pokeballName },
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([{ name: pokeballName, stock: 1 }])
})

it('should decrease the stock of pokeball', async () => {
  // Given
  const pokeballName = 'Poke Ball'
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  const pokeball = new Pokeball({ name: pokeballName, stock: 42 })
  await pokeball.save()

  // When
  await pokeball.decrease()

  // Then
  const pokeballs = await Pokeball.find(
    { name: pokeballName },
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([{ name: pokeballName, stock: 41 }])
})

it('should NOT decrease the stock of pokeball if stock is already 0', async () => {
  // Given
  const pokeballName = 'Poke Ball'
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  const pokeball = new Pokeball({ name: pokeballName, stock: 0 })
  await pokeball.save()

  // When
  await pokeball.decrease()

  // Then
  const pokeballs = await Pokeball.find(
    { name: pokeballName },
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([{ name: pokeballName, stock: 0 }])
})

it('should delete one pokeball', async () => {
  // Given
  const pokeballName = 'Poke Ball'
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  const pokeball = new Pokeball({ name: pokeballName, stock: 42 })
  await pokeball.save()

  // When
  await pokeball.delete()

  // Then
  const pokeballs = await Pokeball.find(
    { name: pokeballName },
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([])
})
