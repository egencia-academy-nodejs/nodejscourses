const { MongoMemoryServer } = require('mongodb-memory-server')
const app = require('./app')
const Pokeball = require('./Pokeball')
const mongoose = require('mongoose')

let mongod

beforeEach(async () => {
  mongod = new MongoMemoryServer()
})

afterEach(async () => {
  mongod.stop()
})

it('should create a single pokeball', async () => {
  // Given
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)

  // When
  await app.createPokeball({ name: 'Poke Ball' })

  // Then
  const pokeballs = await Pokeball.find(
    {},
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([{ name: 'Poke Ball', stock: 0 }])
})

it('should list all pokeballs', async () => {
  // Given
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  await app.createPokeball({ name: 'Poke Ball' })
  await app.createPokeball({ name: 'Master Ball' })
  await app.createPokeball({ name: 'Ultra Ball' })

  // When
  const pokeballs = await app.listAllPokeballs()

  // Then
  expect(pokeballs).toEqual([
    { name: 'Poke Ball', stock: 0 },
    { name: 'Master Ball', stock: 0 },
    { name: 'Ultra Ball', stock: 0 },
  ])
})

it('should increase the stock of pokeball', async () => {
  // Given
  const pokeballName = 'Poke Ball'
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  await app.createPokeball({ name: pokeballName })

  // When
  await app.increasePokeball({ name: pokeballName })

  // Then
  const pokeballs = await Pokeball.find(
    { name: pokeballName },
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([{ name: pokeballName, stock: 1 }])
})

it('should decrease the stock of pokeball', async () => {
  // Given
  const pokeballName = 'Poke Ball'
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  await app.createPokeball({ name: pokeballName })
  await app.increasePokeball({ name: pokeballName })

  // When
  await app.decreasePokeball({ name: pokeballName })

  // Then
  const pokeballs = await Pokeball.find(
    { name: pokeballName },
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([{ name: pokeballName, stock: 0 }])
})

it('should delete one pokeball', async () => {
  // Given
  const pokeballName = 'Poke Ball'
  const uri = await mongod.getConnectionString()
  await mongoose.connect(uri)
  await app.createPokeball({ name: pokeballName })
  await app.increasePokeball({ name: pokeballName })

  // When
  await app.deletePokeball({ name: pokeballName })

  // Then
  const pokeballs = await Pokeball.find(
    { name: pokeballName },
    { _id: 0, name: 1, stock: 1 },
  ).lean()
  expect(pokeballs).toEqual([])
})
