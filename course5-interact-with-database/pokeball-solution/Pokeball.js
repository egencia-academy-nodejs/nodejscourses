const mongoose = require('mongoose')
const Schema = mongoose.Schema

const pokeballSchema = new Schema({
  name: String,
  stock: { type: Number, default: 0 },
})

module.exports = mongoose.model('Pokeball', pokeballSchema)
