const Pokeball = require('./Pokeball')

function createPokeball({ name = 'Poke Ball' }) {
  const pokeball = new Pokeball({ name })
  return pokeball.save()
}

function listAllPokeballs() {
  return Pokeball.find({}, { name: 1, stock: 1, _id: 0 }).lean()
}

function increasePokeball({ name }) {
  if (name) {
    return Pokeball.findOneAndUpdate({ name }, { $inc: { stock: 1 } })
  } else {
    throw new Error('name shoud be defined')
  }
}

function decreasePokeball({ name }) {
  if (name) {
    return Pokeball.findOneAndUpdate({ name }, { $inc: { stock: -1 } })
  } else {
    throw new Error('name shoud be defined')
  }
}

function deletePokeball({ name }) {
  if (name) {
    return Pokeball.deleteMany({ name })
  } else {
    throw new Error('name shoud be defined')
  }
}

module.exports = {
  createPokeball,
  listAllPokeballs,
  increasePokeball,
  decreasePokeball,
  deletePokeball,
}
