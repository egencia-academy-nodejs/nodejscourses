const Pokeball = require('./Pokeball')

function createPokeball({ name = 'Poke Ball' }) {
  // TODO implement me
}

function listAllPokeballs() {
  // TODO implement me
}

function increasePokeball({ name }) {
  // TODO implement me
}

function decreasePokeball({ name }) {
  // TODO implement me
}

function deletePokeball({ name }) {
  // TODO implement me
}

module.exports = {
  createPokeball,
  listAllPokeballs,
  increasePokeball,
  decreasePokeball,
  deletePokeball,
}
